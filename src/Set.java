import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.*;

public class Set {

    static HashMap<String, String> dir_data = new HashMap<>();
    static HashMap<String, String> hr_data = new HashMap<>();
    static HashMap<String, String> mar_data = new HashMap<>();
    static HashMap<String, String> man_data = new HashMap<>();
    static HashMap<String, String> wor_data = new HashMap<>();
    static HashMap<String, String> new_worker1 = new HashMap<>();
    static HashMap<String, String> new_worker2 = new HashMap<>();
    static HashMap<String, String> new_worker3 = new HashMap<>();
    /////// to do list
    static ArrayList<String> toDo_list = new ArrayList<String>();
    static ArrayList<String> toDo_IP = new ArrayList<String>();
    static ArrayList<String> toDo_Done = new ArrayList<String>();
    public static void datas(){
        toDo_Done.add("simple");
        ///////   data's of all users  \\\\\\\\\\\\\\
        // director
        dir_data.put("Status", "Director");
        dir_data.put("Name", "Арсен Бекболотов");
        dir_data.put("experience", "10 years");
        dir_data.put("salary", "50 000");
        //// hr
        hr_data.put("Status", "HR Специалист");
        hr_data.put("Name", "Сергей Иванов ");
        hr_data.put("experience", "7 years");
        hr_data.put("salary", "30000");
        ///// marketer
        mar_data.put("Status", "Marketer");
        mar_data.put("Name", "Айсулуу Боронбаева ");
        mar_data.put("experience", "5 years");
        mar_data.put("salary", "50 000");
        //// manager
        man_data.put("Status", "Manager");
        man_data.put("Name", "Марат Никифоров");
        man_data.put("experience", "4 years");
        man_data.put("salary", "27 000");
        //// worker
        wor_data.put("Status", "Manager");
        wor_data.put("Name", "Марат Никифоров");
        wor_data.put("experience", "4 years");
        wor_data.put("salary", "27 000");
        //// for new workers
        new_worker1.put("Status", "");
        new_worker1.put("Name", "");
        new_worker1.put("experience", "");
        new_worker1.put("salary", "");

        new_worker2.put("Status", "");
        new_worker2.put("Name", "");
        new_worker2.put("experience", "");
        new_worker2.put("salary", "");

        new_worker3.put("Status", "");
        new_worker3.put("Name", "");
        new_worker3.put("experience", "");
        new_worker3.put("salary", "");
    }
    public static void main(String[] args) {
        System.out.println("Пожалуйста, авторизуйтесь");
        datas();
        chooseUser();
    }
    public static void chooseUser() {
        Scanner sc = new Scanner(System.in);
        String [] L = {"Выберите вашу специальность :","(1) Director","(2) Marketer","(3) HR","(4) Manager","(5) Worker","(0) close"};
        for(String l : L){
            System.out.println(l);
        }
        String choice = sc.next();
        switch(choice) {
            case "1":
            case "director":
            case "Director":
                System.out.println("welcome, Director!");
                DirectorLg();
                break;
            case "0":
            case "close":
                System.exit(0);
                break;
            case "2":
            case "marketer":
            case "Marketer":
                System.out.println("welcome, Marketer!");
                MarketerLg();
                break;
            case "3":
            case "hr":
            case "HR":
                System.out.println("welcome, HR!");
                HRLg();
                break;
            case "4":
            case "manager":
            case "Manager":
                System.out.println("welcome, Manager!");
                ManagerLg();
                break;
            case "5":
            case "worker":
            case "Worker":
                System.out.println("welcome, Worker!");
                WorkerLg();
                break;
            default:
                // code
                System.out.println("incorrect choice!");
        }
    }
    ////////////////////////////////////       USER'S LOGIN         \\\\\\\\\\\\\\\\\\
    ////////////   Director Login     \\\\\\\\\\\\\\\\
    public static void DirectorLg(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Write a username:>> ");
        String userName = sc.next();
        System.out.print("Write your password:>> ");
        String userPass = sc.next();
        if(userName.equals("director") && (userPass.equals("admin"))){
            System.out.println("Арсен Бекболотов, вы успешно вошли!");
            DirectorsMenu();
        }
        else{
            System.out.println("Извините, но мы не нашли такой тип аккаунта, пожалуйста повторите.");
            System.out.println("Желаете повторить[0] или выйти[1]? ");
            int ex = sc.nextInt();
            if(ex == 0){
                DirectorLg();
            }else if(ex == 1){
                chooseUser();
            }
        }
    }
    ////////////     Marketer Login    \\\\\\\\\\
    public static void MarketerLg() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Write a username:>> ");
        String userName = sc.next();
        System.out.print("Write your password:>> ");
        String userPass = sc.next();
        if (userName.equals("marketer") && (userPass.equals("admin"))) {
            System.out.println("Айсулуу Боронбаева , вы успешно вошли!");
            MarketersMenu();
        } else {
            System.out.println("Извините, но мы не нашли такой тип аккаунта, пожалуйста повторите.");
            System.out.println("Желаете повторить[0] или выйти[1]? ");
            int ex = sc.nextInt();
            if (ex == 0) {
                MarketerLg();
            } else if (ex == 1) {
                chooseUser();
            }
        }
    }
    //////////        HR  Login      \\\\\\\\\\\
    public static void HRLg() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Write a username:>>");
        String userName = sc.next();
        System.out.print("Write your password:>>");
        String userPass = sc.next();
        if (userName.equals("HR") && (userPass.equals("admin"))) {
            System.out.println("Сергей Иванов, вы успешно вошли!");
        } else {
            System.out.println("Извините, но мы не нашли такой тип аккаунта, пожалуйста повторите.");
            System.out.println("Желаете повторить[0] или выйти[1]? ");
            int ex = sc.nextInt();
            if (ex == 0) {
                HRLg();
            } else if (ex == 1) {
                chooseUser();
            }
        }
    }
    ///////////       Manager Login     \\\\\\\\\\\
    public static void ManagerLg() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Write a username:>>");
        String userName = sc.next();
        System.out.print("Write your password:>>");
        String userPass = sc.next();
        if (userName.equals("Manager") && (userPass.equals("admin"))) {
            System.out.println("Марат Никифоров, вы успешно вошли!");
            ManagersMenu();
        } else {
            System.out.println("Извините, но мы не нашли такой тип аккаунта, пожалуйста повторите.");
            System.out.println("Желаете повторить[0] или выйти[1]? ");
            int ex = sc.nextInt();
            if (ex == 0) {
                ManagerLg();
            } else if (ex == 1) {
                chooseUser();
            }
        }
    }
    ////////////       Worker Login   \\\\\\\\\\\\\
    public static void WorkerLg() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Write a username:>> ");
        String userName = sc.next();
        System.out.print("Write your password:>> ");
        String userPass = sc.next();
        if (userName.equals("worker") && (userPass.equals("admin"))) {
            System.out.println("Андрей Гаврилов, вы успешно вошли!");
            WorkersMenu();
        } else {
            System.out.println("Извините, но мы не нашли такой тип аккаунта, пожалуйста повторите.");
            System.out.println("Желаете повторить[0] или выйти[1]? ");
            int ex = sc.nextInt();
            if (ex == 0) {
                WorkerLg();
            } else if (ex == 1) {
                chooseUser();
            }
        }
    }

    ///////////////////////////      USER'S MENU    \\\\\\\\\\\\\\\\\\
    ////////  Director's menu   \\\\\\\\\\\\\\\\\
    public static void DirectorsMenu(){
        Scanner sc = new Scanner(System.in);
        String [] diMenu = {"Пожалуйста наберите номер меню для работы с программой, если закончили, " +
                "то наберите 9","Menu:","(1) Показать список всех зон покрытия ","(2) Показать список " +
                "категорий бюджета ","(3) Показать выделенный бюджет для определенной категории мест для" +
                " маркетинга ","(4) Показать текущие средства для маркетинга","(5) Показать общий бюджет " +
                "необходимый для зарплаты","(6) Повысить зарплату сотруднику","(7) Понизить зарплату" +
                " сотруднику","(8) Показать список оборудований для строительства объектов","(9) Выход"};
        for(String i : diMenu){
            System.out.println(i);
        }
        String directorsChoise = sc.next();
        switch (directorsChoise){
            case "1":
                break;
            case "2":
                break;
            case "3":
                break;
            case "4":
                break;
            case "5":
                break;
            case "6":
                salary();
                break;
            case "7":
                salary();
                break;
            case "8":
                break;
            case "9":
                System.out.println("Программа завершена, мы будем рады вашему возвращению!");
//                System.exit(9);
                chooseUser();
                break;
            default:
                System.out.println("Wrong number try again");
                DirectorsMenu();
        }
    }
    ////////    Marketer's menu    \\\\\\\\\\\\\\\\\
    public static void MarketersMenu(){
        Scanner sc = new Scanner(System.in);
        String [] marMenu = {"Пожалуйста наберите номер меню для работы с программой, если закончили," +
                " то наберите 6:","Menu:","(1) Показать список всех зон охвата клиентов по регионам ",
                "(2) Показать список категорий для маркетинга ","(3) Показать выделенный бюджет для " +
                "определенной категории мест для маркетинга  ","(4) Показать общий бюджет для " +
                "маркетинга ","(5) Потратить бюджет на продвижение","(6) Выход "};
        for(String i : marMenu){
            System.out.println(i);
        }
        String marketersChoise = sc.next();
        switch (marketersChoise){
            case "1":
                break;
            case "2":
                break;
            case "3":
                break;
            case "4":
                break;
            case "5":
                break;
            case "6":
                System.out.println("Программа завершена, мы будем рады вашему возвращению!");
                System.exit(6);
                break;
            default:
                System.out.println("Wrong number try again");
                MarketersMenu();
        }
    }
    ////////    Manager's menu         \\\\\\\\\\\\\\\\
    public static void ManagersMenu(){
        Scanner sc = new Scanner(System.in);
        String [] manMenu = {"Пожалуйста наберите номер меню для работы с программой, " +
                "если закончили, то наберите 5","Menu:","(2) Показать список дел  ","(3) " +
                "Показать список указаний к сотрудникам ","(4) Показать список всех зон " +
                "покрытия","(5) Выход"};
        for(String i : manMenu){
            System.out.println(i);
        }
        String managersChoise = sc.next();
        switch (managersChoise){
            case "1":
                break;
            case "2":
                break;
            case "3":
                toDo();
                break;
            case "4":
                break;
            case "5":
                System.out.println("Программа завершена, мы будем рады вашему возвращению!");
                chooseUser();
                break;
            default:
                System.out.println("Wrong number try again");
                ManagersMenu();
        }
    }
    ////////     Worker's menu       \\\\\\\\\\\\\\\\\
    public static void WorkersMenu(){
        Scanner sc = new Scanner(System.in);
        String [] woMenu = {"Пожалуйста наберите номер меню для работы с программой, если закончили, " +
                "то наберите 5","Menu:","(1) Показать список порученных мне дел.","(2) Показать список " +
                "завершенных указаний.","(3) Показать список дел над, которым я работаю.","(4) Показать " +
                "зарплату","(5) Выход"};
        for(String i : woMenu){
            System.out.println(i);
        }

        String workersChoise = sc.next();
        switch (workersChoise){
            case "1":
                workT();
                break;
            case "2":
                workD();
                break;
            case "3":
                workI();
                break;
            case "4":
                showSalary();
                break;
            case "5":
                System.out.println("Программа завершена, мы будем рады вашему возвращению!");
//                System.exit(5);
                chooseUser();
                break;
            default:
                System.out.println("Wrong number try again");
                WorkersMenu();
        }
    }
    ///////////////////////      User's Action       \\\\\\\\\\\\\\\\\\\\\
    /////////////// director's action    \\\\\\\\\\\\\\\\\\\\\\\
    public static void salary(){
        Scanner sc = new Scanner(System.in);
        System.out.println("select");
        String wor_sal [] = {"1) Марат Никифоров- Менеджер", "2) Андрей Гаврилов - Сотрудник",
                "3) Айсулуу Боронбаева - Маркетолог","4) Сергей Иванов - HR Специалист","5) Арсен " +
                "Бекболотов - Гендиректор","0) Выход"};
        for(String i : wor_sal){
            System.out.println(i);
        }
        int choice = sc.nextInt();
        if(choice == 0){
            DirectorsMenu();
        }else if(choice == 1){
            System.out.println("Enter a salary:>>");
            String salary = sc.next();
            man_data.replace("salary", salary);
            System.out.println("Done");
            DirectorsMenu();
        }else if(choice == 2){
            System.out.println("Enter a salary:>>");
            String salary = sc.next();
            wor_data.replace("salary", salary);
            System.out.println("Done");
            DirectorsMenu();
        }else if(choice == 3){
            System.out.println("Enter a salary:>>");
            String salary = sc.next();
            mar_data.replace("salary", salary);
            System.out.println("Done");
            DirectorsMenu();
        }else if(choice == 4){
            System.out.println("Enter a salary:>>");
            String salary = sc.next();
            hr_data.replace("salary", salary);
            System.out.println("Done");
            DirectorsMenu();
        }else if(choice == 5){
            System.out.println("Enter a salary:>>");
            String salary = sc.next();
            dir_data.replace("salary", salary);
            System.out.println("Done");
            DirectorsMenu();
        }else{
            System.out.println("Incorrect choice");
            salary();
        }
    }
    //////////////   manager's action    \\\\\\\\\\\\\\\\\\\\\
    public static void toDo(){
        Scanner sc = new Scanner(System.in);
        System.out.println("toDo list:>>");
        for(String i : toDo_list){
            System.out.println(i);
        }
        System.out.println("toDo in progress:>>");
        for(String i : toDo_IP){
            System.out.println(i);
        }
        System.out.println("toDo Done:>>");
        for(String i : toDo_Done){
            System.out.println(i);
        }
        System.out.println("1) add works");
        System.out.println("0) exit");
        int choice = sc.nextInt();
        if(choice == 0){
            ManagersMenu();
        }else if(choice == 1){
            System.out.println("Enter a work");
            String todo = sc.next();
            toDo_list.add(todo);
            System.out.println("Done");
            ManagersMenu();
        }else{
            System.out.println("Incorrect choice!");
            toDo();
        }
    }
    /////////////    worker's action   \\\\\\\\\\\\\\\\\\
    public static void workT(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Select number of work, which you want work on Or click 0 to go menu");
        int j = 1;
        for(String i : toDo_list){
            System.out.println(j+ ") "+ i);
            j++;
        }
        int choice = sc.nextInt();
        if(choice == 0){
            WorkersMenu();
        }else {
            try{
                toDo_IP.add(toDo_list.get(choice-1));
                toDo_list.remove(choice-1);
                System.out.println("selected");
                WorkersMenu();
            }catch (Exception e){
                System.out.println("incorrect!");
                workT();
            }

        }
    }
    public static void workD(){
        Scanner sc = new Scanner(System.in);
        System.out.println("1) search");
        System.out.println("Entere 0 to go menu:");
        int j = 1;
        for(String i : toDo_Done){
            System.out.println(j+ ") "+ i);
        }
        int ex = sc.nextInt();
        if(ex == 0){
            WorkersMenu();
        }else if(ex == 1){
            search();
        }
        else{
            System.out.println("incorrect");
            workD();
        }
    }
    public static void workI(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Select number of work which you done Or click 0 to go menu");
        int j = 1;
        for(String i : toDo_IP){
            System.out.println(j+ ") "+ i);
        }
        int choice = sc.nextInt();
        if(choice == 0){
            WorkersMenu();
        }else{
            try{
                toDo_Done.add(toDo_IP.get(choice-1));
                toDo_IP.remove(choice-1);
                System.out.println("selected");
                WorkersMenu();
            }catch (Exception e){
                System.out.println("error!");
                workI();
            }

        }
    }
    public static void showSalary(){
        Scanner sc = new Scanner(System.in);
        System.out.println(wor_data.get("salary"));
        System.out.println("Enter a 0 to exit");
        int ex = sc.nextInt();
        if(ex == 0){
            WorkersMenu();
        }else{
            System.out.println("Incorrect");
            showSalary();
        }

    }
    public static void search(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a text to search:>>");
        String text = sc.next();
        text.toLowerCase();
        for(String i : toDo_Done){
            i.toLowerCase();
            if(i.contains(text)){
                System.out.println(i);
                System.out.println("0)exit");
            }
        }
    }
}

